hgwk-launcher, by MonkeysAreEvil
https://bitbucket.org/MonkeysAreEvil/hgwk-launcher

# Description

An aesthetically minimal app launcher for GNU/Linux. Particularly designed for awesome window manager. Contains widgets for launching applications, either through a command or shortcut icon; displaying the current time and calender; displaying battery and power information; and a widget for shutdown, restart, suspend, lock and log out. 

# Screenshots

![Application launcher](screenshots/applications01.png)

![Application launcher](screenshots/applications02.png)

![Application launcher](screenshots/applications03.png)

![Date and time information](screenshots/datetime.png)

![Power management](screenshots/power.png)


# Installation

See `install.sh` for an example.

## Steps (short version)

* Install dependencies
* `$ mkdir build && cd build && cmake ../src && make`

# Configuration

hgwk-launcher requires a configuration folder at `$HOME/.config/hgwk-launcher`. A complete configuration is:

```
.
+-- config
|	+-- stylesheet.qss
+-- scripts
|	+-- system-lock-screen.sh
|	+-- system-log-out.sh
|	+-- system-poweroff.sh
|	+-- system-reboot.sh
|	+-- system-suspend.sh
+-- static
|	+-- foo.svg
|	+-- ...
+-- hgwk-launcher.conf
+-- shortcuts
```

A minimal configuration contains just `shortcuts`

```
.
+-- shortcuts
```

* `hgwk-launcher.conf` contains general, default configurations. It defines what functionality is enabled and the locations and details of necessary dbus and scripts. Some of these options can be overridden with command-line arguments. Optional.
	- `[widgets]` enables and disables functionality
	- `[scripts]` defines the full paths of the scripts used for power management. If these paths are empty, dbus is used instead for power management. If these paths are defined, dbus will not be used for power management. This is important: on a systemd distro, logind will be installed and dbus can be used to shutdown etc. Otherwise, one has to use `sudo shutdown` etc. In this case, `/etc/sudoers` should be configured so no password is needed (or some similar procedure) e.g. `user ALL=(root) NOPASSWD: /sbin/poweroff`.
	- All subsequent sections define dbus configuration. If they don't work, you'll have to poke around with `dbus-monitor` and the various other dbus tools.
* `config/stylesheet.qss` defines, in a CSS-like syntax, styling for the Qt widgets. Optional.
* `scripts` contains bash scripts which execute the appropriate power function. Optional; conceivably could be python or binary too.
* `static` contains svg icon files. These override the system icons. Qt5 can't reliably load icons from the system theme.
* `shortcuts` contains a list of applications to be displayed in the main application widget. hgwk-launcher will read this file and populate the applications widget with shortcut icons to these applications. It will first try and load an icon from `$HOME/.config/hgwk-launcher/static/<application>.svg`. If that cannot be found, it tries to load an icon from the system theme matching the application name. If there is no system icon, a dummy error icon should be displayed. Example:

```
qutebrowser
firefox
dolphin
gimp
pavucontrol
rhythmbox
steam
virtualbox
```

hgwk-launcher just simply runs and hangs around. The best way to manage it is to use something like the [scratchpad manager](https://awesomewm.org/wiki/Scratchpad_manager) to access it. This lets you toggle display and hiding hgwk-launcher using a keystroke:
* Install scratchpad manager as documented
* Configure awesome to toggle displaying hgwk-launcher. Add something like the following to your keybindings section of your rc.lua:
```lua
-- Launcher
awful.key({ modkey }, "space", function () scratch.drop("/home/<user>/bin/hgwk-launcher", "top", "left", 300, 1
080, true, 1) end),
```
* Reload awesome

# Usage

## Arguments

Several command line arguments are available:
```
  -h, --help                Displays this help.
  -v, --version             Displays version information.
  -a, --disable-apps        Don't display the app launcher.
  -b, --disable-battery     Don't display the battery info.
  -c, --disable-clock       Don't display the clock and calender.
  -p, --disable-power       Don't display the power buttons.
  -w, --disable-networking  Don't display the networking config and info.
  -s, --lock-screen <path>  Path to lock screen script.
  -l, --logout <path>       Path to logout script.
  -r, --reboot <path>       Path to reboot script.
  -z, --suspend <path>      Path to suspend script.
```

These options can be used to hide widgets you don't use. For example, the battery widgets are only really useful on a laptopand. Hence it is useful to hide these widgets. Note that hiding a widget will not effect start up time. 

## Application

- Click an icon to launch it. hgwk-launcher will blindly try and launch the application based on the name in your `shortcuts.rc` file.
- Search for an application. This will filter the displayed shortcut icons, only displaying shortcuts matching the search string. It's a little clever; it will match based on substrings.
- Launch an application by command. If you want to launch an application not in your `shortcuts.rc`, just 'search' the application binary name. hgwk-launcher will search your `/usr/share/applications/*.desktop` files and prompt suggestions matching your search string. You can launch more complicated commands; there is no intelligence in how applications are launched. For example, all the following strings will be launched successfully:
```
firefox
kdesude firefox
firefox -profile /path/to/profile
```
## Clock Widget

All this does is show the current time and calender. Nothing fancy here.

## Battery Widget

All this does is show an icon representing the current battery state (i.e. full, 50% full, charging). It is intended to also display more useful information, such as battery capacity and time to charge.

## Power Widget

Provides shortcuts to shutdown, suspend, restart, log off and lock screen, as displayed top to bottom. These icons are extracted from your icon theme. The commands run are defined by the scripts in the config folder. This means you can run several commands before shutting down. I'm also lazy and haven't bothered with systemd integration.

## Networking Widget

Incomplete and may never be finished, in lieu of `nmtui`. Permanently disabled: config option and command line arg will not enable this.

# Contributing

I'm happy to take suggestions but right now I'm not prepared to take pull requests or other contributions. This is a hobby project and will be infrequently updated. Of course it's free software so feel free to go nuts if you want (under the GPLv3 terms, naturally).

# Notes

My laptops buggered at the moment so no screenshots of the battery widget.


# License

hgwk-launcher is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

hgwk-launcher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with hgwk-launcher.  If not, see <http://www.gnu.org/licenses/>.
