#!/bin/bash

echo "Preparing configs"
config=$HOME/.config/hgwk-launcher
mkdir -p "$config"
touch "$config/shortcuts"
cp -a config/ "$config/config/"
cp -a scripts/ "$config/scripts"
mkdir "$config/static"

echo "Installing dependencies"
if [ -x "$(command -v apt)" ]
then
	sudo apt install cmake build-essential qtbase5-dev
fi
if [ -x "$(command -v emerge)" ]
then
	sudo emerge -n --quiet cmake qt-meta
fi

echo "Building"
mkdir build
cd build || exit
cmake ../src
make

sudo cp build/hgwk-launcher /usr/local/bin/
