/***************************************************************************
 * file: accessPoint.cpp												   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: incomplete implementation, part of an attempt to include 		   *
 * 		  network management functionality.								   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "accessPoint.h"

accessPoint::accessPoint(void)
{
	accessPoint::isConnected = false;
	accessPoint::Flags = -1;
	accessPoint::WpaFlags =-1;
	accessPoint::RsnFlags =-1;
	accessPoint::Ssid = "";
	accessPoint::Frequency = -1;
	accessPoint::HwAddress = "";
	accessPoint::Mode = -1;
	accessPoint::MaxBitrate = -1;
	accessPoint::Strength = -1;
	accessPoint::LastSeen = -1;
}

accessPoint::~accessPoint()
{
	accessPoint::isConnected = false;
	accessPoint::Flags = -1;
	accessPoint::WpaFlags =-1;
	accessPoint::RsnFlags =-1;
	accessPoint::Ssid = "";
	accessPoint::Frequency = -1;
	accessPoint::HwAddress = "";
	accessPoint::Mode = -1;
	accessPoint::MaxBitrate = -1;
	accessPoint::Strength = -1;
	accessPoint::LastSeen = -1;
}
