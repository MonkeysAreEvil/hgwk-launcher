/***************************************************************************
 * file: accessPointFlags.cpp											   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: incomplete implementation, part of an attempt to include 		   *
 * 		  network management functionality.								   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "accessPointFlags.h"

accessPointFlags::accessPointFlags(void)
{
}

accessPointFlags::~accessPointFlags()
{
}

QString accessPointFlags::readDetail(int flag)
{
	QString rc = "";
	if (flag == AP_FLAGS_NONE)
		rc = "Access point has no special capabilities.";
	if (flag == AP_FLAGS_PRIVACY)
		rc = "Access point requires authentication and encryption (usually WEP).";

	return rc;
}
