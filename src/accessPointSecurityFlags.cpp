/***************************************************************************
 * file: accessPointSecurityFlags.cpp									   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: incomplete implementation, part of an attempt to include 		   *
 * 		  network management functionality.								   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "accessPointSecurityFlags.h"

accessPointSecurityFlags::accessPointSecurityFlags(void)
{
}

accessPointSecurityFlags::~accessPointSecurityFlags()
{
}

QString accessPointSecurityFlags::readDetail(int flag)
{
	QString rc = "";

	if (flag == 0)
		rc = "The access point has no special security requirements.";

	if ((flag & AP_SEC_NONE) != 0)
		rc += "The access point has no special security requirements.\n";
	if ((flag & AP_SEC_PAIR_WEP40) != 0)
		rc += "40/64-bit WEP is supported for pairwise/unicast encryption.\n";
	if ((flag & AP_SEC_PAIR_WEP104) != 0)
		rc += "104/128-bit WEP is supported for pairwise/unicast encryption.\n";
	if ((flag & AP_SEC_PAIR_TKIP) != 0)
		rc += "TKIP is supported for pairwise/unicast encryption.\n";
	if ((flag & AP_SEC_PAIR_CCMP) != 0)
		rc += "AES/CCMP is supported for pairwise/unicast encryption.\n";
	if ((flag & AP_SEC_GROUP_WEP40) != 0)
		rc += "40/64-bit WEP is supported for group/broadcast encryption.\n";
	if ((flag & AP_SEC_GROUP_WEP104) != 0)
		rc += "104/128-bit WEP is supported for group/broadcast encryption.\n";
	if ((flag & AP_SEC_GROUP_TKIP) != 0)
		rc += "TKIP is supported for group/broadcast encryption.\n";
	if ((flag & AP_SEC_GROUP_CCMP) != 0)
		rc += "AES/CCMP is supported for group/broadcast encryption.\n";
	if ((flag & AP_SEC_KEY_MGMT_PSK) != 0)
		rc += "WPA/RSN pre-shared key encryption is supported.\n";
	if ((flag & AP_SEC_KEY_MGMT_802_1X) != 0)
		rc += "802.1x authentication and key management is supported.\n";

	return rc;
}
