/***************************************************************************
 * file: accessPointSecurityFlags.h										   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: incomplete implementation, part of an attempt to include 		   *
 * 		  network management functionality.								   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef AP_SECURITY_FLAGS_H
#define AP_SECURITY_FLAGS_H

#include <QtWidgets>

enum securityFlags
{
	AP_SEC_NONE = 0,
	AP_SEC_PAIR_WEP40 = 1,
	AP_SEC_PAIR_WEP104 = 2,
	AP_SEC_PAIR_TKIP = 4,
	AP_SEC_PAIR_CCMP = 8,
	AP_SEC_GROUP_WEP40 = 10,
	AP_SEC_GROUP_WEP104 = 20,
	AP_SEC_GROUP_TKIP = 40,
	AP_SEC_GROUP_CCMP = 80,
	AP_SEC_KEY_MGMT_PSK = 100,
	AP_SEC_KEY_MGMT_802_1X = 200
};

class accessPointSecurityFlags
{
	public:
		accessPointSecurityFlags();
		virtual ~accessPointSecurityFlags();
		QString readDetail(int);
};   

#endif
