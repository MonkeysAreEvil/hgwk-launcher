/***************************************************************************
 * file: main-window.cpp												   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: build the gui and do all the main things						   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/


#include "main-window.h"
#include "accessPointFlags.h"
#include "accessPointSecurityFlags.h"
#include <QComboBox>
#include <QPalette>
#include <QtWidgets>
#include <QDir>
#include <QDirIterator>
#include <QTextStream>
#include <QDBusMessage>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusArgument>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QNetworkAddressEntry>
#include <QValidator>
#include <QProcess>
#include <QSettings>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <dirent.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <stdio.h>
#include <string> 
#include <algorithm>

bool fileExists(QString path) {
	QFileInfo check_file(path);
	if (check_file.exists() && check_file.isFile()) {
		return true;
	} else {
		return false;
	}
}

MainWindow::MainWindow(bool apps, bool battery, bool clock, bool power, bool networking, QString lockScreenPath, QString logoutPath, QString poweroffPath, QString rebootPath, QString suspendPath)
{   
	QCoreApplication::setOrganizationName("hgwk-launcher");
	QCoreApplication::setOrganizationDomain("hgwk-launcher.com");
	QCoreApplication::setApplicationName("hgwk-launcher");

	buildConfig(apps, battery, clock, power, networking, lockScreenPath, logoutPath, poweroffPath, rebootPath, suspendPath);
	configWatcher = new QFileSystemWatcher(this);
	configWatcher->addPath(MainWindow::configPath + "config/hgwk-launcher.conf");
	connect(configWatcher, SIGNAL(fileChanged(QString)), this, SLOT(configChanged(QString)));

	buildUI();

	searchBox->setFocus();
}


// Qt handles most of the memory management because of widget hierachy.
MainWindow::~MainWindow()
{
	delete centralWidget;
}

// Used for splitting some std::string
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) 
{
	std::stringstream ss(s);
	std::string item;

	while (std::getline(ss, item, delim)) 
	{
		elems.push_back(item);
	}

	return elems;
}


// Calls a given bash script and returns stdout to a std::string
std::string MainWindow::exec(std::string script, bool stripNewLine)
{
	const char *theScript = script.c_str();

	std::shared_ptr<FILE> pipe(popen(theScript, "r"), pclose);

	if (!pipe)
	{
		return "ERROR";
	}

	char buffer[4096];
	std::string result = "";
	while (!feof(pipe.get()))
	{
		if (fgets(buffer, 4096, pipe.get()) != NULL)
		{
			result += buffer;
		}
	}

	if (stripNewLine == true)
	{
		result.erase(std::remove(result.begin(), result.end(), '\n'), result.end());
	}

	return result;
}


// Reads a given .desktop file and extracts the icon name.
QString MainWindow::getAppIcon(QString appExe)
{
	QString icon;
	QFile app("/usr/share/applications/" + appExe + ".desktop");

	if (!app.open(QIODevice::ReadOnly | QIODevice::Text))	
	{
		icon = "error";
	}
	else
	{
		QTextStream in(&app);
		while (!in.atEnd()) 
		{
			QString line = in.readLine();

			if (line.contains("Icon"))
			{
				QStringList iconList = line.split('=');
				icon = iconList[1];
				break;
			}
		}
	}

	return icon;
}


void MainWindow::clearSearch()
{
	MainWindow::searchBox->clear();
}


// Used to generate suggestions/completions for the search text box
// Searches through the .desktop files in /usr/share/applications and builds a list of installed applications
QStringList MainWindow::buildCompleter()
{
	QStringList wordList;

	QDirIterator appsDir("/usr/share/applications", QDirIterator::Subdirectories);
	while (appsDir.hasNext())
	{
		QDir dir(appsDir.next());
		dir.setFilter(QDir::NoDotAndDotDot | QDir::Files);
		QFileInfoList appsList = dir.entryInfoList();

		for (int i = 0; i < appsList.size(); ++i)
		{
			QFileInfo app = appsList.at(i);
			wordList << app.baseName();
		}	
	}

	return wordList;
}


void MainWindow::appsChanged(QString directory)
{
	QStringList wordList;
	wordList = buildCompleter();
	completer = new QCompleter(wordList, this);
	completer->setCaseSensitivity(Qt::CaseInsensitive);
	completer->setCompletionMode(QCompleter::InlineCompletion);
	searchBox->setCompleter(completer);
}

void MainWindow::configChanged(QString file)
{
	bool apps = false,
		 battery = false,
		 clock = false,
		 power = false,
		 networking = false;

	if (readConfig(MainWindow::configuration, "widgets/apps", "").compare("disabled") == 0)
		apps = true;

	if (readConfig(MainWindow::configuration, "widgets/battery", "").compare("disabled") == 0)
		battery = true;

	if (readConfig(MainWindow::configuration, "widgets/clock", "").compare("disabled") == 0)
		clock = true;

	if (readConfig(MainWindow::configuration, "widgets/power", "").compare("disabled") == 0)
		power = true;

	if (readConfig(MainWindow::configuration, "widgets/networking", "").compare("disabled") == 0)
		networking = true;

	QString lockScreenPath = readConfig(MainWindow::configuration, "scripts/lock-screen", "");
	QString logoutPath = readConfig(MainWindow::configuration, "scripts/logout", "");
	QString poweroffPath = readConfig(MainWindow::configuration, "scripts/poweroff", "");
	QString rebootPath = readConfig(MainWindow::configuration, "scripts/reboot", "");
	QString suspendPath = readConfig(MainWindow::configuration, "scripts/suspend", "");

	MainWindow::configuration.clear();
	buildConfig(apps, battery, clock, power, networking, lockScreenPath, logoutPath, poweroffPath, rebootPath, suspendPath);
}


// As text is typed in the search text box, filter the displayed icons.
// Given some search string, check if it matches the icon name. Hide if it doesn't
void MainWindow::filterShortcuts()
{
	QWidget* widget;
	std::string searchString = MainWindow::searchBox->text().toUtf8().constData();

	QString iconName;
	std::string iconName_string;
	std::map<std::string,std::string> shortcutsToHide;

	// Reshow all icons when text is changed
	for (int i = 0; i < this->shortcutsWidget->layout()->count(); ++i)
	{
		widget = this->shortcutsWidget->layout()->itemAt(i)->widget();
		widget->show();
	}

	for (int i = 0; i < this->shortcutsWidget->layout()->count(); ++i)		// For each shortcut button...
	{
		widget = this->shortcutsWidget->layout()->itemAt(i)->widget();
		iconName = widget->accessibleName();								// No easy way to extract the icon name or assigned exe, so store value in the accessible field (side effect of being useful to screenreaders).
		iconName_string = iconName.toUtf8().constData();
		std::size_t found = iconName_string.find(searchString);				// ...check if the searched string matches the button name...

		if (found == std::string::npos)										// ....and hide the button if it does not match...
		{
			widget->hide();
		}		
		else
		{
			this->shortcutsWidget->layout()->setAlignment(Qt::AlignTop);	// ...or move the widget to the top of the layour, so it's formatted nicely.
		}	
	}
}	


bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
	// Overwrite the default tab behaviour of the search text box.
	// Catches the keypress event, and filters for the tab key.
	if (obj == searchBox) 
	{
		if (event->type() == QEvent::KeyPress) 
		{
			QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
			if (keyEvent->key() == Qt::Key_Tab)
			{
				// When tab key is pressed, move cursor to end of line, selecting suggestion.
				this->searchBox->end(false);

				return true;
			}             
		}
		return QMainWindow::eventFilter(obj, event);
	}


	// Overwrite the default mouse over event and add a nice hover effect
	if (strcmp(obj->metaObject()->className(), "QPushButton") == 0)
	{
		if (event->type() == QEvent::Enter)
		{
			QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect();
			effect->setOpacity(0.6);

			QPushButton *button = qobject_cast<QPushButton *>(obj); 
			button->setGraphicsEffect(effect);

			return true;
		}

		if (event->type() == QEvent::Leave)
		{
			QGraphicsOpacityEffect *effect = new QGraphicsOpacityEffect();
			effect->setOpacity(1);

			QPushButton *button = qobject_cast<QPushButton *>(obj); 
			button->setGraphicsEffect(effect);

			return true;
		}
		return QMainWindow::eventFilter(obj, event);
	}
	return false;
}


// When shortcut buttons are pressed, run the associated application.
void MainWindow::launchShortcut(QString exe)
{
	QProcess application;
	bool rc = application.startDetached(exe);

	if (!rc)
	{
		QProcess *notify = new QProcess(this);
		notify->start("notify-send", QStringList() << "Error starting application");
	}

	// Clear the searchbox after launching
	MainWindow::searchBox->clear();
}


// When the enter key is pressed in the search text box, run the string typed.
void MainWindow::searchText()
{
	QString searchText = this->searchBox->displayText();

	QProcess application;
	bool rc = application.startDetached(searchText);

	if (!rc)
	{
		QProcess *notify = new QProcess(this);
		notify->start("notify-send", QStringList() << "Error starting application");
	}

	// Clear the searchbox after launching
	MainWindow::searchBox->clear();
}


// Run the appropriate power script when clicking a power button
void MainWindow::lock()
{
	std::string systemLockScreenScript = readConfig(MainWindow::configuration, "scripts/lock-screen", "").toUtf8().constData();
	system(systemLockScreenScript.c_str());
}


void MainWindow::logout()
{
	std::string systemLogoutScript = readConfig(MainWindow::configuration, "scripts/logout", "").toUtf8().constData();
	system(systemLogoutScript.c_str());
}


void MainWindow::powerOff()
{
	std::string systemPoweroffScript = readConfig(MainWindow::configuration, "scripts/poweroff", "").toUtf8().constData();
	if (systemPoweroffScript.length() != 0)
	{
		system(systemPoweroffScript.c_str());
	}
	else
	{
		QDBusConnection bus = QDBusConnection::systemBus();
		QString service = readConfig(MainWindow::configuration, "poweroff/service", "org.freedesktop.login1");
		QString path = readConfig(MainWindow::configuration, "poweroff/path", "/org/freedesktop/login1");
		QString interface = readConfig(MainWindow::configuration, "poweroff/interface", "org.freedesktop.login1.Manager");
		QString method = readConfig(MainWindow::configuration, "poweroff/method", "PowerOff");
		QDBusInterface face(service, path, interface, bus);
		face.call(method, true);
	}

}


void MainWindow::reboot()
{
	std::string systemRebootScript = readConfig(MainWindow::configuration, "scripts/reboot", "").toUtf8().constData();
	if (systemRebootScript.length() != 0)
	{
		system(systemRebootScript.c_str());
	}
	else
	{
		QDBusConnection bus = QDBusConnection::systemBus();
		QString service = readConfig(MainWindow::configuration, "reboot/service", "org.freedesktop.login1");
		QString path = readConfig(MainWindow::configuration, "reboot/path", "/org/freedesktop/login1");
		QString interface = readConfig(MainWindow::configuration, "reboot/interface", "org.freedesktop.login1.Manager");
		QString method = readConfig(MainWindow::configuration, "reboot/method", "Reboot");
		QDBusInterface face(service, path, interface, bus);
		face.call(method, true);
	}
}


void MainWindow::suspend()
{
	std::string systemSuspendScript = readConfig(MainWindow::configuration, "scripts/suspend", "").toUtf8().constData();
	if (systemSuspendScript.length() != 0)
	{
		system(systemSuspendScript.c_str());
	}
	else
	{
		QDBusConnection bus = QDBusConnection::systemBus();
		QString service = readConfig(MainWindow::configuration,"suspend/service", "org.freedesktop.login1");
		QString path = readConfig(MainWindow::configuration,"suspend/path", "/org/freedesktop/login1");
		QString interface = readConfig(MainWindow::configuration,"suspend/interface", "org.freedesktop.login1.Manager");
		QString method = readConfig(MainWindow::configuration,"suspend/method", "Suspend");
		QDBusInterface face(service, path, interface, bus);
		face.call(method, true);
	}
}


// Update clock etc labels to match correct time
void MainWindow::updateClock()
{
	updateClockButton();
	updateTimeLabel();
	updateDateLabel();
}

void MainWindow::updateClockButton()
{
	std::string theTime = updateTime("%H:%M");

	QString theTime2 = QString::fromStdString(theTime);

	this->clockButton->setText(theTime2);
}


void MainWindow::updateTimeLabel()
{
	std::string theTime = updateTime("%H:%M:%S");

	QString theTime2 = QString::fromStdString(theTime);

	this->timeLabel->setText(theTime2);
}


void MainWindow::updateDateLabel()
{
	std::string theTime = updateTime("%A, %d %B %Y");

	QString theTime2 = QString::fromStdString(theTime);

	this->dateLabel->setText(theTime2);
}


// Returns the current date/time in some given format
std::string MainWindow::updateTime(std::string format)
{
	auto now = std::chrono::system_clock::now();
	auto nowTime = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&nowTime), format.c_str());
	std::string nowTime2 = ss.str();

	return nowTime2;
}

QList<accessPoint> MainWindow::findAPs()
{
	QList<accessPoint> accessPointList;

	QSettings settings;
	const QString service = settings.value("networking/service", "org.freedesktop.NetworkManager").toString();
	const QString path = settings.value("networking/path", "/org/freedesktop/NetworkManager/AccessPoint").toString();
	const QString interface = settings.value("networking/interface", "org.freedesktop.NetworkManager.AccessPoint").toString();

	QDBusConnection bus = QDBusConnection::systemBus();

	// Find the path that stores the active connection
	QString activeConnectionPath = getActiveConnectionPath();

	// If there is no active connection
	if (activeConnectionPath == "/org/freedesktop/NetworkManager/ActiveConnection" || activeConnectionPath == "/org/freedesktop/NetworkManager/ActiveConnection/")
		return accessPointList;

	// Retrieve the SSID of the active connection
	QString activeConnectionSSID = getActiveConnectionSSID(activeConnectionPath);

	QDBusMessage message = QDBusMessage::createMethodCall(service, path, "org.freedesktop.DBus.Introspectable", "Introspect");
	QDBusMessage rc = bus.call(message);
	QString xml = rc.arguments()[0].value<QString>();

	QXmlStreamReader reader(xml);
	while (!reader.atEnd() && !reader.hasError())
	{
		if (reader.readNext() == QXmlStreamReader::StartElement && reader.name() == "node")
		{
			QString accessPointID = reader.attributes().value("name").toString();
			if (accessPointID != "")
			{
				QString accessPointPath = path + "/" + accessPointID;

				// Prepare to retrieve properties
				QList<QVariant> args;
				args.append(QVariant(interface));
				QDBusMessage message, rc;

				// Retrieve flags. Describes the access point caccessPointabilities
				args.append(QVariant("Flags"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto flags = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve WPA flags. Describes the security and authentication requirements
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("WpaFlags"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto wpaFlags = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve RSN flags. Describes the security and authentication requirements
				// Note that this may differ from WpaFlags
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("RsnFlags"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto rsnFlags = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve SSID
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("Ssid"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto ssid = rc.arguments()[0].value<QDBusVariant>().variant().value<QString>();

				// Retrieve frequency of the AP
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("Frequency"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto frequency = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve the MAC/hardware address of the AP 
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("HwAddress"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto hwAddress = rc.arguments()[0].value<QDBusVariant>().variant().value<QString>();

				// Retrieve mode of the AP
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("Mode"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto mode = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve bitrate of the AP
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("MaxBitrate"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto maxBitrate = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve signal strength, in percent
				// Higher numbers mean stronger signal
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("Strength"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto strength = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve the timestamp (in CLOCK_BOOTTIME) seconds of the last time tha AP was found
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("LastSeen"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto lastSeen = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Check if connected and set icon
				bool isConnected = false;
				QString icon = "network-wireless-offline";
				if (activeConnectionSSID.compare(ssid) == 0)
				{
					icon = "network-wireless-signal-excellent";
					isConnected = true;
				}

				// Record AP details
				accessPoint newAP;
				newAP.isConnected = isConnected;
				newAP.icon = icon;
				newAP.Flags = flags;
				newAP.WpaFlags = wpaFlags;
				newAP.RsnFlags = rsnFlags;
				newAP.Ssid = ssid;
				newAP.Frequency = frequency;
				newAP.HwAddress = hwAddress;
				newAP.Mode = mode;
				newAP.MaxBitrate = maxBitrate;
				newAP.Strength = strength;
				newAP.LastSeen = lastSeen;

				accessPointList.append(newAP);
			}
		}
	}


	return accessPointList;
}

// Change the battery icon to match the appropriate level
void MainWindow::getBatteryInfo()
{
	QString batteryIcon;
	int percentage = getBatteryPercentage();
	bool pluggedIn = isPluggedIn();

	if (pluggedIn == false)
	{
		if (percentage >= 0 && (percentage < 10))
		{
			batteryIcon = "battery-000";
		} else if (percentage >= 10 && (percentage < 20))
		{
			batteryIcon = "battery-010";
		} else if (percentage >= 20 && (percentage < 30))
		{
			batteryIcon = "battery-020";
		} else if (percentage >= 30 && (percentage < 40))
		{
			batteryIcon = "battery-030";
		} else if (percentage >= 40 && (percentage < 50))
		{
			batteryIcon = "battery-040";
		} else if (percentage >= 50 && (percentage < 60))
		{
			batteryIcon = "battery-050";
		} else if (percentage >= 60 && (percentage < 70))
		{
			batteryIcon = "battery-060";
		} else if (percentage >= 70 && (percentage < 80))
		{
			batteryIcon = "battery-070";
		} else if (percentage >= 80 && (percentage < 90))
		{
			batteryIcon = "battery-080";
		} else if (percentage >= 90 && (percentage < 100))
		{
			batteryIcon = "battery-090";
		} else if (percentage == 0)
		{
			batteryIcon = "battery-000";
		} else if (percentage == 100)
		{	
			batteryIcon = "battery-100";
		}
	}
	else if (pluggedIn == true)
	{
		if (percentage >= 0 && (percentage < 10)) 
		{
			batteryIcon = "battery-000-charging";
		} else if (percentage >= 10 && (percentage < 20))
		{
			batteryIcon = "battery-010-charging";
		} else if (percentage >= 20 && (percentage < 30))
		{
			batteryIcon = "battery-020-charging";
		} else if (percentage >= 30 && (percentage < 40))
		{
			batteryIcon = "battery-030-charging";
		} else if (percentage >= 40 && (percentage < 50))
		{
			batteryIcon = "battery-040-charging";
		} else if (percentage >= 50 && (percentage < 60))
		{
			batteryIcon = "battery-050-charging";
		} else if (percentage >= 60 && (percentage < 70))
		{
			batteryIcon = "battery-060-charging";
		} else if (percentage >= 70 && (percentage < 80))
		{
			batteryIcon = "battery-070-charging";
		} else if (percentage >= 80 && (percentage < 90))
		{
			batteryIcon = "battery-080-charging";
		} else if (percentage >= 90 && (percentage < 100))
		{
			batteryIcon = "battery-090-charging";
		} else if (percentage == 0)
		{
			batteryIcon = "battery-000-charging";
		}
		else if (percentage == 100)
		{
			batteryIcon = "battery-100-charging";
		}
	}

	if (batteryIcon == "")
	{
		batteryIcon = "battery-error";
	}

	// Temporary hack: use icons as defined in path not from theme as Qt + KDE does not correctly get icons from theme.
	//batteryButton->setIcon(QIcon::fromTheme(batteryIcon));
	batteryButton->setIcon(QIcon(MainWindow::configPath + "static/" + batteryIcon + ".svg"));
}

//Get more detailed information about the battery
void MainWindow::batteryGetInfo()
{
	getBatteryInfo();
	long timeToEmptySeconds = getBatteryTimeToEmpty();
	long timeToFullSeconds = getBatteryTimeToFull();

	QString capacity = QString::number(getBatteryCapacity());
	QString energy = QString::number(getBatteryEnergy());
	QString energyFull = QString::number(getBatteryEnergyFull());
	QString energyFullDesign = QString::number(getBatteryEnergyFullDesign());
	QString energyRate = QString::number(getBatteryEnergyRate());
	QString percentage = QString::number(getBatteryPercentage());
	QString voltage = QString::number(getBatteryVoltage());
	QString timeToEmpty = QString::number(convertTimeToDecimal(convertNumbersToTime(0,0,timeToEmptySeconds)));;
	QString timeToFull = QString::number(convertTimeToDecimal(convertNumbersToTime(0,0,timeToFullSeconds)));

	QString batteryInfo = ""
		"<b>Capacity:            </b>" + capacity+ "%<hr>"
		"<b>Energy:              </b>" + energy + " Wh<hr>"
		"<b>Energy Full:         </b>" + energyFull + " Wh<hr>"
		"<b>Energy Full (design):</b> " + energyFullDesign + " Wh<hr>"
		"<b>Energy Rate:         </b>" + energyRate + " W<hr>"
		"<b>Percentage:          </b>" + percentage + "%<hr>"
		"<b>Voltage:             </b>" + voltage + "V<hr>"
		"<b>Time to Empty:       </b>" + timeToEmpty + " hours<hr>"
		"<b>Time to Full:        </b>" + timeToFull + " hours<hr>";

	this->batteryLargeWidget->setText(batteryInfo);
}

QDBusMessage MainWindow::getBatteryInformation(QString property)
{
	QSettings settings;
	const QString service = readConfig(MainWindow::configuration, "battery/service", "org.freedesktop.UPower");
	const QString path = readConfig(MainWindow::configuration, "battery/path", "/org/freedesktop/UPower/devices/battery_BAT0");
	const QString interface = readConfig(MainWindow::configuration, "battery/interface", "org.freedesktop.UPower.Device");
	const QString propertiesInterface = readConfig(MainWindow::configuration, "battery:information-properties/interface", "org.freedesktop.DBus.Properties");
	const QString method = readConfig(MainWindow::configuration, "battery:information-properties/method", "Get");

	QDBusConnection bus = QDBusConnection::systemBus();
	QList<QVariant> args;
	args.append(QVariant(interface));
	args.append(QVariant(property));

	QDBusMessage mesg = QDBusMessage::createMethodCall(service, path, propertiesInterface, method);
	mesg.setArguments(args);
	QDBusMessage ret = bus.call(mesg);

	return ret;
}

// Return the current battery percentage using D-Bus
double MainWindow::getBatteryPercentage()
{
	QDBusMessage ret = getBatteryInformation("Percentage");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

double MainWindow::getBatteryCapacity()
{
	QDBusMessage ret = getBatteryInformation("Capacity");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

double MainWindow::getBatteryEnergy()
{
	QDBusMessage ret = getBatteryInformation("Energy");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

double MainWindow::getBatteryEnergyFull()
{
	QDBusMessage ret = getBatteryInformation("EnergyFull");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

double MainWindow::getBatteryEnergyFullDesign()
{
	QDBusMessage ret = getBatteryInformation("EnergyFullDesign");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

double MainWindow::getBatteryEnergyRate()
{
	QDBusMessage ret = getBatteryInformation("EnergyRate");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

double MainWindow::getBatteryVoltage()
{
	QDBusMessage ret = getBatteryInformation("Voltage");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

int64_t MainWindow::getBatteryTimeToEmpty()
{
	QDBusMessage ret = getBatteryInformation("TimeToEmpty");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

int64_t MainWindow::getBatteryTimeToFull()
{
	QDBusMessage ret = getBatteryInformation("TimeToFull");
	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<double>();

	return value;
}

bool MainWindow::isPluggedIn()
{
	QSettings settings;
	const QString service = readConfig(MainWindow::configuration, "battery:is-plugged-in/service", "org.freedesktop.UPower");
	const QString path = readConfig(MainWindow::configuration, "battery:is-plugged-in/path", "/org/freedesktop/UPower/devices/battery_BAT0");
	const QString interface = readConfig(MainWindow::configuration, "battery:is-plugged-in/interface", "org.freedesktop.UPower.Device");
	const QString property = readConfig(MainWindow::configuration, "battery:is-plugged-in/property", "Online");
	const QString informationInterface = readConfig(MainWindow::configuration, "battery:information-properties/interface", "org.freedesktop.DBus.Properties");
	const QString informationMethod = readConfig(MainWindow::configuration, "battery:information-properties/method", "Get");


	QDBusConnection bus = QDBusConnection::systemBus();
	QList<QVariant> args;
	args.append(QVariant(interface));
	args.append(QVariant(property));

	QDBusMessage mesg = QDBusMessage::createMethodCall(service, path, informationInterface, informationMethod);
	mesg.setArguments(args);
	QDBusMessage ret = bus.call(mesg);

	double value = ret.arguments()[0].value<QDBusVariant>().variant().value<bool>();

	return value;
}

// Converts integers to a time
QTime MainWindow::convertNumbersToTime(int64_t hours, int64_t minutes, int64_t seconds)
{
	// Use clock arithmetic to increment minutes and hours as required
	long minutesIncr = 0, 
		 secondsOut = 0, 
		 hourOut = 0, 
		 minutesOut = 0;

	if (seconds > 60)
	{
		minutesIncr = (seconds / 60) + minutes;
		secondsOut = seconds & 60;
	}

	if (minutesIncr > 60)
	{
		hourOut = (minutesIncr / 60) + hours;
		minutesOut = minutesIncr % 60;
	}


	QTime *rc = new QTime(hourOut,minutesOut,secondsOut);

	return *rc;
}

double MainWindow::convertTimeToDecimal(QTime time)
{
	int hours = time.hour();
	int minutes = time.minute();

	return hours + (floor(minutes / 6) / 10);
}

void MainWindow::toggleNetworkingOptions(bool checked)
{
	if (checked)
	{
		this->ipAddress->setDisabled(false);
		this->netmask->setDisabled(false);
		this->gateway->setDisabled(false);
		this->ipAddressLabel->setDisabled(false);
		this->netmaskLabel->setDisabled(false);
		this->gatewayLabel->setDisabled(false);
	}
	else
	{
		this->ipAddress->setDisabled(true);
		this->netmask->setDisabled(true);
		this->gateway->setDisabled(true);
		this->ipAddressLabel->setDisabled(true);
		this->netmaskLabel->setDisabled(true);
		this->gatewayLabel->setDisabled(true);
	}
}

void MainWindow::setOptionsWidget(QListWidgetItem *widget)
{
	// Retrieve the widget matching the selected item
	QString ssid = widget->text();

	// The SSID links the item and options panel
	for (int i = 0; i < this->accessPointOptions->layout()->count(); ++i)	
	{
		QWidget *option = this->accessPointOptions->layout()->itemAt(i)->widget();
		QString accessPointName = option->accessibleName();

		if (ssid.compare(accessPointName) == 0)
		{
			QMetaObject::invokeMethod(this->accessPointOptions, "setCurrentIndex", Q_ARG(int, i));
			break;
		}
	}
}

QString MainWindow::getActiveConnectionPath()
{
	QString activeConnectionPath = "/org/freedesktop/NetworkManager/ActiveConnection", 
			activeConnectionID = "";
	const QString service = "org.freedesktop.NetworkManager";

	QDBusConnection bus = QDBusConnection::systemBus();
	QDBusMessage message = QDBusMessage::createMethodCall(service, activeConnectionPath, "org.freedesktop.DBus.Introspectable", "Introspect");
	QDBusMessage rc = bus.call(message);
	QString xml = rc.arguments()[0].value<QString>();
	QXmlStreamReader reader(xml);
	while (!reader.atEnd() && !reader.hasError())
	{
		if (reader.readNext() == QXmlStreamReader::StartElement && reader.name() == "node")
			activeConnectionID = reader.attributes().value("name").toString();
	}

	return activeConnectionPath + "/" + activeConnectionID;
}

QString MainWindow::getActiveConnectionSSID(QString activeConnectionPath)
{
	const QString service = "org.freedesktop.NetworkManager";
	QDBusConnection bus = QDBusConnection::systemBus();

	QDBusMessage message = QDBusMessage::createMethodCall(service, activeConnectionPath, "org.freedesktop.DBus.Properties", "Get");
	QList<QVariant> args;
	args.append(QVariant("org.freedesktop.NetworkManager.Connection.Active"));
	args.append("Id");
	message.setArguments(args);
	QDBusMessage rc = bus.call(message);
	auto activeConnectionSSID = rc.arguments()[0].value<QDBusVariant>().variant().value<QString>();

	return activeConnectionSSID;
}

QString MainWindow::getIPAddress()
{
	QString ipAddress = "";
	foreach (const QHostAddress &address, QNetworkInterface::allAddresses())
	{
		if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
			ipAddress = address.toString();
	}	

	return ipAddress;
}

QString MainWindow::getNetMask()
{
	QString netmask = "";
	QNetworkAddressEntry hostAddress;
	foreach (const QHostAddress &address, QNetworkInterface::allAddresses())
	{
		if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
		{
			hostAddress.setIp(address);
			netmask = hostAddress.netmask().toString();
		}
	}	

	return netmask;
}

QString MainWindow::getGateway()
{
	QString gateway = "";

	// Ignore the documentation pipes don't work in Qt so hack around it
	QProcess process;
	process.start("bash", QStringList() << "-c" << "ip route show | grep -i 'default via' | awk '{print $3}'");

	if (!process.waitForStarted())
		return gateway;

	bool rc = false;
	QByteArray buffer;
	while ((rc = process.waitForFinished()))
		buffer.append(process.readAll());

	gateway = buffer;

	return gateway;
}

void MainWindow::buildUI()
{
	// The window has one large central widget
	centralWidget = new QWidget();
	centralLayout = new QHBoxLayout(centralWidget);

	// This is split into a left-hand panel and a right-hand
	leftPanel = new QWidget(this);
	rightPanel = new QStackedWidget(this);
	leftLayout = new QVBoxLayout(leftPanel);
	leftPanel->setObjectName("leftPanel");

	// Display functionality if configured
	// The left side contains simple icons
	// When the icons a clicked, more information and functionality is displayed on the right
	if (readConfig(MainWindow::configuration, "widgets/apps", "").compare("enabled") == 0)
		buildApps();
	if (readConfig(MainWindow::configuration, "widgets/clock", "").compare("enabled") == 0)
		buildClock();
	if (readConfig(MainWindow::configuration, "widgets/battery", "").compare("enabled") == 0)
		buildBattery();
	if (readConfig(MainWindow::configuration, "widgets/power", "").compare("enabled") == 0)
		buildPower();
	// if (readConfig(MainWindow::configuration, "widgets/networking", "").compare("enabled") == 0)
		// buildNetworking();

	leftPanel->setLayout(leftLayout);
	centralLayout->addWidget(leftPanel);
	centralLayout->addWidget(rightPanel);
	centralWidget->setLayout(centralLayout);
	centralLayout->setContentsMargins(0,0,0,0);

	setCentralWidget(centralWidget);
}

// Displays the app launcher
void MainWindow::buildApps()
{
	// A button for the left-hand panel
	appsButton = new QPushButton(this);

	// A container for the app launchers
	appsExtraWidget = new QWidget(this);
	appsExtraLayout = new QVBoxLayout(appsExtraWidget);

	// A widget that can filter the shortcut icons and launch apps
	searchWidget = new QWidget(this);
	searchBox = new QLineEdit(this);

	// A container that lists shortcuts (as icons)
	shortcutsWidget = new QWidget(this);
	shortcutsScrollArea = new QScrollArea(this);
	shortcutsLayout = new QVBoxLayout(shortcutsScrollArea);

	// Identify the widget for styling
	appsExtraWidget->setObjectName("appsExtraWidget");

	// Style the left-hand button
	appsButton->setFlat(true);
	appsButton->setAutoFillBackground(true);
	appsButton->setFixedHeight(60);
	appsButton->setFixedWidth(60);
	appsButton->setIconSize(QSize(60,60));
	appsButton->setIcon(QIcon(MainWindow::configPath + "/static/app-launcher.svg"));
	appsButton->installEventFilter(this);

	// Gets a list of all the gui applications installed by scanning for .desktop files
	// Use this list to show suggestions when using the search bar
	// Use a QFileSystemWatcher to update the autocompleter when a new app is installed
	appsWatcher = new QFileSystemWatcher(this);
	appsWatcher->addPath("/usr/share/applications");
	connect(appsWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(appsChanged(QString)));
	QStringList wordList;
	wordList = buildCompleter();
	completer = new QCompleter(wordList, this);
	completer->setCaseSensitivity(Qt::CaseInsensitive);
	completer->setCompletionMode(QCompleter::InlineCompletion);
	searchBox->setCompleter(completer);

	// I override the default tab event to move to the end of the line rather than focus the next widget.
	// This means that tab completes the search.	
	searchBox->installEventFilter(this);
	searchBox->setPlaceholderText("search");
	searchBox->setTextMargins(30,15,10,10);
	searchBox->setObjectName("searchBox");
	searchBox->setPlaceholderText("Search");

	// Add a nice, KDE-like button that clears the search when clicked
	QPalette clearPalette(palette());
	clearPalette.setColor(QPalette::Button, Qt::transparent);
	clearButton = new QPushButton(this);
	clearButton->setPalette(clearPalette);
	clearButton->setFlat(true);
	clearButton->setAutoFillBackground(true);
	searchLayout = new QHBoxLayout(searchWidget);
	clearButton->setIcon(QIcon::fromTheme("edit-clear"));

	// As the user types in the search box, filter the displayed shortcut icons
	connect(searchBox, SIGNAL(textChanged(const QString&)), this, SLOT(filterShortcuts()));

	// When the user hits enter, run the entered search text
	connect(searchBox, SIGNAL(returnPressed()), this, SLOT(searchText()));

	// Clear the search text when the user clicks the clear button
	connect(clearButton, SIGNAL(clicked()), this, SLOT(clearSearch()));

	// Start putting all of the widgets together
	searchLayout->addWidget(searchBox);
	searchLayout->addWidget(clearButton);
	appsExtraLayout->addWidget(searchWidget);

	// Nice little separator between search box and shortcuts
	line = new QFrame;
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	appsExtraLayout->addWidget(line);


	// Use the config file to build shortcut icons 
	QFile config(MainWindow::configPath + "shortcuts");
	if (!config.open(QIODevice::ReadOnly | QIODevice::Text))	
	{
		// no config file
	}
	else
	{
		// Read each line in the shortcuts config file and generate a shortcut button
		// Config file is just a list executable names
		QTextStream in(&config);
		while (!in.atEnd()) 
		{
			QString exe = in.readLine();
			QString icon = getAppIcon(exe);
			QSignalMapper *shortcutsSM = new QSignalMapper(this);	
			QPushButton *shortcutButton = new QPushButton(shortcutsScrollArea);

			shortcutButton->setFlat(true);
			shortcutButton->setAutoFillBackground(true);
			shortcutButton->setFixedHeight(200);
			shortcutButton->setFixedWidth(200);
			shortcutButton->setIconSize(QSize(200,200));
			shortcutButton->setAccessibleName(icon);		// Used when filtering the buttons. Stores the icon name because there's no easy way to extract that.

			// Prefer custom icon if it exists
			// Otherwise, use the theme
			// Because icon themes and Qt are a pile of bullshit
			QString customIcon = MainWindow::configPath + "/static/" + exe + ".svg";
			if (fileExists(customIcon)) {
				shortcutButton->setIcon(QIcon(customIcon));
			} else {
				shortcutButton->setIcon(QIcon::fromTheme(icon, QIcon(customIcon)));
			}
			shortcutButton->installEventFilter(this);

			shortcutsLayout->addWidget(shortcutButton);

			// Clicking the shortcut launches the app
			connect(shortcutButton, SIGNAL(clicked()), shortcutsSM, SLOT(map()));
			shortcutsSM->setMapping(shortcutButton, exe);
			connect(shortcutsSM, SIGNAL(mapped(QString)), this, SLOT(launchShortcut(QString)));
		}
	}

	// Clicking the left-hand button shows the shortcuts page
	appsButtonSM = new QSignalMapper;
	connect(appsButton, SIGNAL(clicked()), appsButtonSM, SLOT(map()));
	appsButtonSM->setMapping(appsButton, appsExtraWidget);
	connect(appsButtonSM, SIGNAL(mapped(QWidget *)), rightPanel, SLOT(setCurrentWidget(QWidget *)));

	// Continue putting all the widgets together
	// The list of shortcuts needs a scrollbar
	shortcutsScrollArea->setWidgetResizable(true);
	shortcutsWidget->setObjectName("shortcutsWidget");
	shortcutsWidget->setLayout(shortcutsLayout);
	appsExtraLayout->addWidget(shortcutsScrollArea);
	shortcutsScrollArea->setWidget(shortcutsWidget);
	appsExtraWidget->setLayout(appsExtraLayout);
	this->leftLayout->addWidget(appsButton);
	this->rightPanel->addWidget(appsExtraWidget);
}

// Displays a clock and calender
void MainWindow::buildClock()
{
	// A button for the left hand panel
	clockButton = new QPushButton(this);

	// A container for thhe right-hand panel
	clockExtraWidget = new QWidget(this);
	clockExtraLayout = new QVBoxLayout(clockExtraWidget);
	clockExtraWidget->setAutoFillBackground(true);

	// Styling
	clockExtraWidget->setObjectName("clockExtraWidget");
	clockButton->setFixedWidth(60);
	clockButton->setFixedHeight(60);
	clockButton->setFlat(true);
	clockButton->setAutoFillBackground(true);
	clockButton->setObjectName("clockButton");

	// The left-hand panel is a simple clock
	clockTimer = new QTimer(this);

	// Update the clock and date every second
	connect(clockTimer, SIGNAL(timeout()), this, SLOT(updateClock()));
	clockTimer->start(1000);
	clockTimer->installEventFilter(this);	// Show a nice mouseover effect

	// Show a larger clock, the date, and a calender in the right-hand panel
	timeLabel = new QLabel(this);
	dateLabel = new QLabel(this);
	calendar = new QCalendarWidget(this);
	timeLabel->setObjectName("timeLabel");
	dateLabel->setObjectName("dateLabel");
	// timeLabel->setMaximumHeight(50);
	dateLabel->setMaximumWidth(250);
	dateLabel->setWordWrap(true);
	calendar->setMaximumHeight(200);

	// Put all the widgets together
	QLabel *buffer = new QLabel;
	buffer->setFixedHeight(700);
	clockExtraLayout->setAlignment(Qt::AlignTop);
	clockExtraLayout->addWidget(timeLabel);
	clockExtraLayout->addWidget(dateLabel);
	clockExtraLayout->addWidget(calendar);
	clockExtraLayout->addWidget(buffer);
	clockExtraWidget->setLayout(clockExtraLayout);

	// Clicking on the left-hand clock button shows the calender etc
	clockButtonSM = new QSignalMapper;
	connect(clockButton, SIGNAL(clicked()), clockButtonSM, SLOT(map()));
	clockButtonSM->setMapping(clockButton, clockExtraWidget);
	connect(clockButtonSM, SIGNAL(mapped(QWidget *)), rightPanel, SLOT(setCurrentWidget(QWidget *)));

	this->leftLayout->addWidget(clockButton);
	this->rightPanel->addWidget(clockExtraWidget);
}

// Displays battery information
void MainWindow::buildBattery()
{
	// A button for the left-hand panel
	batteryButton = new QPushButton(this);

	// A container for the right-hand panel
	batteryExtraWidget = new QWidget(this);
	batteryExtraLayout = new QVBoxLayout(batteryExtraWidget);

	// Styling
	batteryExtraWidget->setAutoFillBackground(true);
	batteryExtraWidget->setObjectName("batteryExtraWidget");
	batteryButton->setFlat(true);
	batteryButton->setFixedWidth(60);
	batteryButton->setFixedHeight(60);
	batteryButton->setAutoFillBackground(true);
	batteryButton->setIconSize(QSize(60,60));
	batteryButton->installEventFilter(this);

	// The left-hand button is assigned an icon that represents the current battery capacity and state (charging/discharging)
	getBatteryInfo();

	// Display some useful information about the battery state
	batteryLargeWidget = new QLabel;
	batteryLargeWidget->setObjectName("batteryLargeWidget");
	batteryLargeWidget->setWordWrap(true);
	batteryGetInfo();

	// Update the battery information when new information is available
	const QString service = readConfig(MainWindow::configuration, "battery:changed/service", "org.freedesktop.UPower");
	const QString batteryPath = readConfig(MainWindow::configuration, "battery:changed/batteryPath", "/org/freedesktop/UPower/devices/battery_BAT0");
	const QString acPath = readConfig(MainWindow::configuration, "battery:changed/acPath", "/org/freedesktop/UPower/devices/line_power_AC0");
	const QString interface = readConfig(MainWindow::configuration, "battery:changed/interface", "org.freedesktop.DBus.Properties");
	const QString property = readConfig(MainWindow::configuration, "battery:changed/property", "PropertiesChanged");

	QDBusConnection bus = QDBusConnection::systemBus();

	bus.connect(service, acPath, interface, property, this, SLOT(batteryGetInfo()));
	bus.connect(service, batteryPath, interface, property, this, SLOT(batteryGetInfo()));

	// Put all the widgets together
	batteryExtraLayout->setAlignment(Qt::AlignTop);
	batteryExtraLayout->addWidget(batteryLargeWidget);
	batteryExtraWidget->setLayout(batteryExtraLayout);

	// Clicking the left-hand button displays battery information
	batteryButtonSM = new QSignalMapper;
	connect(batteryButton, SIGNAL(clicked()), batteryButtonSM, SLOT(map()));
	batteryButtonSM->setMapping(batteryButton, batteryExtraWidget);
	connect(batteryButtonSM, SIGNAL(mapped(QWidget *)), rightPanel, SLOT(setCurrentWidget(QWidget *)));

	this->leftLayout->addWidget(batteryButton);
	this->rightPanel->addWidget(batteryExtraWidget);
}

// Display some useful power buttons
void MainWindow::buildPower()
{
	// A button for the left-hand panel
	powerButton = new QPushButton(this);

	// A container for the right-hand panel
	powerExtraWidget = new QWidget(this);
	powerExtraLayout = new QVBoxLayout(powerExtraWidget);

	// Styling
	powerExtraWidget->setAutoFillBackground(true);
	powerExtraWidget->setObjectName("powerExtraWidget");
	powerButton->setFlat(true);
	powerButton->setAutoFillBackground(true);
	powerButton->setFixedWidth(60);
	powerButton->setFixedHeight(60);
	powerButton->setIconSize(QSize(60,60));
	powerButton->setIcon(QIcon(MainWindow::configPath + "static/system-shutdown.svg"));
	powerButton->installEventFilter(this);

	// Buttons for each power activity
	lockButton = new QPushButton;
	logoutButton = new QPushButton;
	powerOffButton = new QPushButton;
	rebootButton = new QPushButton;
	suspendButton = new QPushButton;

	// Styling
	// There is an event filter so that when the mouse is over a button it looks highlighted (manipulates opacity)	
	lockButton->setFlat(true);
	lockButton->setAutoFillBackground(false);
	lockButton->setFixedHeight(160);
	lockButton->setFixedWidth(160);
	lockButton->setIconSize(QSize(150,150));
	lockButton->setIcon(QIcon(MainWindow::configPath + "static/system-lock-screen.svg"));
	lockButton->installEventFilter(this);

	logoutButton->setFlat(true);
	logoutButton->setAutoFillBackground(false);
	logoutButton->setFixedHeight(160);
	logoutButton->setFixedWidth(160);
	logoutButton->setIconSize(QSize(160,160));
	logoutButton->setIcon(QIcon(MainWindow::configPath + "static/system-log-out.svg"));
	logoutButton->installEventFilter(this);

	powerOffButton->setFlat(true);
	powerOffButton->setAutoFillBackground(false);
	powerOffButton->setFixedHeight(160);
	powerOffButton->setFixedWidth(160);
	powerOffButton->setIconSize(QSize(150,150));
	powerOffButton->setIcon(QIcon(MainWindow::configPath + "static/system-shutdown.svg"));
	powerOffButton->installEventFilter(this);

	rebootButton->setFlat(true);
	rebootButton->setAutoFillBackground(false);
	rebootButton->setFixedHeight(160);
	rebootButton->setFixedWidth(160);
	rebootButton->setIconSize(QSize(150,150));
	rebootButton->setIcon(QIcon(MainWindow::configPath + "static/system-reboot.svg"));
	rebootButton->installEventFilter(this);

	suspendButton->setFlat(true);
	suspendButton->setAutoFillBackground(false);
	suspendButton->setFixedHeight(160);
	suspendButton->setFixedWidth(160);
	suspendButton->setIconSize(QSize(150,150));
	suspendButton->setIcon(QIcon(MainWindow::configPath + "static/system-suspend.svg"));
	suspendButton->installEventFilter(this);

	// Clicking a power button performs the expected action
	connect(lockButton, SIGNAL(clicked()), this, SLOT(lock()));
	connect(logoutButton, SIGNAL(clicked()), this, SLOT(logout()));
	connect(powerOffButton, SIGNAL(clicked()), this, SLOT(powerOff()));
	connect(rebootButton, SIGNAL(clicked()), this, SLOT(reboot()));
	connect(suspendButton, SIGNAL(clicked()), this, SLOT(suspend()));

	// Put all the widgets together
	powerExtraLayout->addWidget(powerOffButton);
	powerExtraLayout->addWidget(suspendButton);
	powerExtraLayout->addWidget(rebootButton);
	powerExtraLayout->addWidget(logoutButton);
	powerExtraLayout->addWidget(lockButton);
	powerExtraWidget->setLayout(powerExtraLayout);

	// Clicking the left-hand button displays the right-hand panel
	powerButtonSM = new QSignalMapper;
	connect(powerButton, SIGNAL(clicked()), powerButtonSM, SLOT(map()));
	powerButtonSM->setMapping(powerButton, powerExtraWidget);
	connect(powerButtonSM, SIGNAL(mapped(QWidget *)), rightPanel, SLOT(setCurrentWidget(QWidget *)));

	this->leftLayout->addWidget(powerButton);
	this->rightPanel->addWidget(powerExtraWidget);
}

// Display information about and manage network connectivity
void MainWindow::buildNetworking()
{
	// A button for the left-hand panel
	networkingButton = new QPushButton(this);

	// A container for the right-hand panel
	networkingExtraWidget = new QStackedWidget(this);

	// Styling
	networkingExtraWidget->setAutoFillBackground(true);
	networkingButton->setFlat(true);
	networkingButton->setFixedWidth(60);
	networkingButton->setFixedHeight(60);
	networkingButton->setAutoFillBackground(true);
	networkingButton->setIconSize(QSize(48,48));
	networkingButton->installEventFilter(this);

	// The left-hand button is assigned an icon that represents the current networking state
	setNetworkIcon();

	// Show a list of available wireless networks
	// Provide options for changing simple network options
	QString networkingStyleSheet = "QListWidget, QListWidgetItem, QTabWidget {font-family: Raleway; font-weight: thin; font-size: 18pt; color: #df253f; border: none;}";
	networkingExtraWidget->setStyleSheet(networkingStyleSheet);
	accessPointList = new QListWidget;
	accessPointOptions = new QStackedWidget;
	networkingOptions = new QWidget;
	networkingOptionsLayout = new QVBoxLayout;
	dhcpStatic = new QRadioButton("Switch to static");
	ipAddress = new QLineEdit;
	netmask = new QLineEdit;
	gateway = new QLineEdit;
	ipAddressLabel = new QLabel;
	netmaskLabel = new QLabel;
	gatewayLabel = new QLabel;
	networkContainerLayout = new QGridLayout;
	networkContainer = new QWidget;

	networkingOptions->setObjectName("networkingOptions");
	networkingOptionsLayout->setAlignment(Qt::AlignTop);
	accessPointOptions->setObjectName("accessPointOptions");

	QList<accessPoint> accessPoints = findAPs();

	// Order by SSID alphabetically
	std::sort(accessPoints.begin(), accessPoints.end(),
			[](const accessPoint a, const accessPoint b) -> bool { return a.Ssid.compare(b.Ssid, Qt::CaseInsensitive) < 0; });
	QListIterator<accessPoint> accessPointIterator(accessPoints);

	// Show the Access Points
	int counter = 0;
	while (accessPointIterator.hasNext())
	{
		accessPoint theAccessPoint = accessPointIterator.next();
		counter++;
		QString ssid = theAccessPoint.Ssid;
		QString icon = theAccessPoint.icon;

		QWidget *accessPointOption = new QWidget;
		QWidget *forwardBack = new QWidget;
		QLabel *accessPointName = new QLabel;
		QLabel *accessPointFlagsLabel = new QLabel;
		QLabel *accessPointWpaFlags = new QLabel;
		QLabel *accessPointRsnFlags = new QLabel;
		QVBoxLayout *accessPointOptionLayout = new QVBoxLayout;
		QHBoxLayout *forwardBackLayout = new QHBoxLayout;
		QPushButton *back = new QPushButton;
		QPushButton *forward = new QPushButton;
		QSignalMapper *backSM = new QSignalMapper;
		QSignalMapper *forwardSM = new QSignalMapper;
		QSignalMapper *forwardSM2 = new QSignalMapper;

		accessPointOption->setStyleSheet("background-color:white;");
		accessPointOptionLayout->setAlignment(Qt::AlignTop);

		new QListWidgetItem(QIcon::fromTheme(icon, QIcon(MainWindow::configPath + "/static/" + icon + ".svg")), ssid, accessPointList);

		accessPointName->setText(ssid);

		// if wpa and rsn flags are the same, don't show rsn flags
		accessPointFlags flags;
		accessPointSecurityFlags securityFlags;
		accessPointFlagsLabel->setObjectName("accessPointFlags");
		accessPointWpaFlags->setObjectName("accessPointWpaFlags");
		accessPointRsnFlags->setObjectName("accessPointRsnFlags");
		accessPointFlagsLabel->setText(flags.readDetail(theAccessPoint.Flags));
		accessPointWpaFlags->setText(securityFlags.readDetail(theAccessPoint.WpaFlags));
		if (theAccessPoint.WpaFlags != theAccessPoint.RsnFlags)
			accessPointRsnFlags->setText(securityFlags.readDetail(theAccessPoint.RsnFlags));

		QLineEdit *accessPointUsername = new QLineEdit;
		accessPointUsername->setPlaceholderText("Username");

		QLineEdit *accessPointPassword = new QLineEdit;
		accessPointPassword->setPlaceholderText("Password");

		QComboBox *accessPointSecurity = new QComboBox;
		QStringList securityOptions;
		securityOptions << "None" << "WEP" << "Dynamic WEP" << "WPA-PSK" << "Ad-Hoc WPA-PSK" << "WPA Enterprise";
		accessPointSecurity->addItems(securityOptions);

		back->setText("< Back");
		forward->setText("Forward >");
		forwardBackLayout->addWidget(back);
		forwardBackLayout->addWidget(forward);
		forwardBack->setLayout(forwardBackLayout);

		connect(back, SIGNAL(clicked()), backSM, SLOT(map()));
		backSM->setMapping(back, 0);
		connect(backSM, SIGNAL(mapped(int)), networkingExtraWidget, SLOT(setCurrentIndex(int)));

		connect(forward, SIGNAL(clicked()), forwardSM, SLOT(map()));
		forwardSM->setMapping(forward, 0);
		connect(forwardSM, SIGNAL(mapped(int)), networkingExtraWidget, SLOT(setCurrentIndex(int)));

		connect(forward, SIGNAL(clicked()), forwardSM2, SLOT(map()));
		forwardSM2->setMapping(forward, ssid);
		connect(forwardSM2, SIGNAL(mapped(QString)), this, SLOT(connectToNetwork(QString)));

		accessPointOptionLayout->addWidget(accessPointName);
		accessPointOptionLayout->addWidget(accessPointUsername);
		accessPointOptionLayout->addWidget(accessPointPassword);
		accessPointOptionLayout->addWidget(accessPointSecurity);
		accessPointOptionLayout->addWidget(accessPointFlagsLabel);
		accessPointOptionLayout->addWidget(accessPointWpaFlags);
		accessPointOptionLayout->addWidget(accessPointRsnFlags);
		accessPointOptionLayout->addWidget(forwardBack);
		accessPointOption->setLayout(accessPointOptionLayout);

		// Abuse this to identify widget
		accessPointOption->setAccessibleName(ssid);
		accessPointOptions->addWidget(accessPointOption);
	}

	networkingExtraWidget->addWidget(accessPointList);
	networkingExtraWidget->addWidget(accessPointOptions);

	// Show the options panel
	networksSM = new QSignalMapper;
	connect(accessPointList, SIGNAL(itemActivated(QListWidgetItem *)), networksSM, SLOT(map()));
	networksSM->setMapping(accessPointList, 1);
	connect(networksSM, SIGNAL(mapped(int)), networkingExtraWidget, SLOT(setCurrentIndex(int)));

	// Open the relevant options page when selecting an access point 
	connect(accessPointList, SIGNAL(itemActivated(QListWidgetItem *)), this, SLOT(setOptionsWidget(QListWidgetItem *)));


	// Change the stack to show the appropriate widget when clicking icons on the left hand panel
	networkingButtonSM = new QSignalMapper;
	connect(networkingButton, SIGNAL(clicked()), networkingButtonSM, SLOT(map()));
	networkingButtonSM->setMapping(networkingButton, networkingExtraWidget);
	connect(networkingButtonSM, SIGNAL(mapped(QWidget *)), rightPanel, SLOT(setCurrentWidget(QWidget *)));

	this->leftLayout->addWidget(networkingButton);
	this->rightPanel->addWidget(networkingExtraWidget);
}

// Retrieve a list of network devices
// Retrieve an icon based on device properties
void MainWindow::setNetworkIcon()
{
	QString networkingIcon;
	QList<networkDevice> deviceList = findNetworkDevices();

	// loop over devices
	QListIterator<networkDevice> devicesIterator(deviceList);

	while (devicesIterator.hasNext())
	{
		networkDevice device = devicesIterator.next();

		if (device.type == NM_DEVICE_TYPE_ETHERNET)
		{
			networkingIcon = "network-wired";

			// Wired takes priority over wireless; only show a wired icon regardless of the other devices
			break;
		}

		if (device.type == NM_DEVICE_TYPE_WIFI)
		{
			if (device.strength >= 0)
				networkingIcon = "network-wireless-signal-poor";
			if (device.strength >= 25)
				networkingIcon = "network-wireless-signal-weak";
			if (device.strength >= 50)
				networkingIcon = "network-wireless-signal-good";
			if (device.strength >= 75)
				networkingIcon = "network-wireless-signal-excellent";
		}
	}

	this->networkingButton->setIcon(QIcon::fromTheme(networkingIcon, QIcon(MainWindow::configPath + "/static/" + networkingIcon + ".svg")));
}

// Retrieve a list of network devices
QList<networkDevice> MainWindow::findNetworkDevices()
{
	QList<networkDevice> deviceList;

	QSettings settings;
	const QString service = settings.value("networking/service", "org.freedesktop.NetworkManager").toString();
	const QString path = "/org/freedesktop/NetworkManager/Devices";
	const QString ipConfigPath = "/org/freedesktop/NetworkManager/IP4Config";
	const QString interface = "org.freedesktop.NetworkManager.Device";

	QDBusConnection bus = QDBusConnection::systemBus();

	QDBusMessage message = QDBusMessage::createMethodCall(service, path, "org.freedesktop.DBus.Introspectable", "Introspect");
	QDBusMessage rc = bus.call(message);
	QString xml = rc.arguments()[0].value<QString>();

	QXmlStreamReader reader(xml);
	while (!reader.atEnd() && !reader.hasError())
	{
		if (reader.readNext() == QXmlStreamReader::StartElement && reader.name() == "node")
		{
			QString deviceID = reader.attributes().value("name").toString();
			if (deviceID != "")
			{
				QString devicePath = path + "/" + deviceID;

				// Prepare to retrieve properties
				QList<QVariant> args;
				args.append(QVariant(interface));
				QDBusMessage message, rc;

				// Retrieve device type i.e. wired or wireless.
				args.append(QVariant("DeviceType"));
				message = QDBusMessage::createMethodCall(service, devicePath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				auto deviceType = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();

				// Retrieve connection state
				args.clear();
				args.append(QVariant("org.freedesktop.NetworkManager.IP4Config"));
				bool connectionState = false;
				// If wired, the device is connected if it has an IP address and a gateway
				if (deviceType == NM_DEVICE_TYPE_ETHERNET)
				{
					// Check for IP address
					args.append(QVariant("AddressData"));
					message = QDBusMessage::createMethodCall(service, ipConfigPath + "/" + deviceID, "org.freedesktop.DBus.Properties", "Get");
					message.setArguments(args);
					rc = bus.call(message);

					// The IP address is deeply buried. Takes a bit of work to unbox it
					const QDBusArgument result = rc.arguments()[0].value<QDBusVariant>().variant().value<QDBusArgument>();
					QString address;
					result.beginArray();
					while (!result.atEnd())
					{
						QList<QPair<QString, QVariant>> addressBoxed;
						result >> addressBoxed;
						address = addressBoxed[0].second.value<QString>();
					}
					result.endArray();

					if (address != "")
						connectionState = true;
				}

				// Prepare a default strength
				int strength = 0;

				// If wireless, the device is connected if there is an active SSID
				if (deviceType == NM_DEVICE_TYPE_WIFI)
				{
					// Find the path that stores the active connection
					QString activeConnectionPath = getActiveConnectionPath();

					// Retrieve the SSID of the active connection
					QString activeConnectionSSID = getActiveConnectionSSID(activeConnectionPath);

					if (activeConnectionSSID != "")
						connectionState = true;

					// If wireless, there should be a strength
					strength = getActiveConnectionStrength(activeConnectionSSID);
				}
				args.clear();

				// Record new device
				networkDevice newDevice;
				newDevice.type = deviceType;	
				newDevice.connected = connectionState;
				newDevice.strength = strength;

				deviceList.append(newDevice);
			}
		}
	}

	return deviceList;
}

// Expects a search of the form "battery/interface"
QString MainWindow::readConfig(QList<config> configuration, QString search, QString defaultValue)
{
	QString searchFamily = search.split("/").first();
	QString searchKey = search.split("/").last();
	QString value = defaultValue;

	QListIterator<config> configIterator(configuration);
	while (configIterator.hasNext())
	{
		config theConfig = configIterator.next();
		if (searchFamily.compare(theConfig.family) == 0 && searchKey.compare(theConfig.key) == 0)
		{
			if (theConfig.value != "")
				value = theConfig.value;
			break;
		}
	}

	return value;
}

void MainWindow::buildConfig(bool apps, bool battery, bool clock, bool power, bool networking, QString lockScreenScriptPath, QString logoutScriptPath, QString poweroffScriptPath, QString rebootScriptPath, QString suspendScriptPath)
{
	QSettings settings;

	MainWindow::home = std::getenv("HOME");
	MainWindow::configPath = home + "/.config/hgwk-launcher/";

	QString widgetAppsValue = settings.value("widgets/apps", "enabled").toString();
	config widgetApps("widgets", "apps", widgetAppsValue);
	if (apps)
		widgetApps.value = "disabled";

	QString widgetBatteryValue = settings.value("widgets/battery", "enabled").toString();
	config widgetBattery("widgets", "battery", widgetBatteryValue);
	if (battery)
		widgetBattery.value = "disabled";

	QString widgetClockValue = settings.value("widgets/clock", "enabled").toString();
	config widgetClock("widgets", "clock", widgetClockValue);
	if (clock)
		widgetClock.value = "disabled";

	QString widgetPowerValue = settings.value("widgets/power", "enabled").toString();
	config widgetPower("widgets", "power", widgetPowerValue);
	if (power)
		widgetPower.value = "disabled";

	QString widgetNetworkingValue = settings.value("widgets/networking", "enabled").toString();
	config widgetNetworking("widgets", "networking", widgetNetworkingValue);
	if (networking)
		widgetNetworking.value = "disabled";

	QString scriptsLockScreenValue = settings.value("scripts/lock-screen", "").toString();
	config scriptsLockScreen("scripts", "lock-screen", scriptsLockScreenValue);
	if (lockScreenScriptPath != "")
		scriptsLockScreen.value = lockScreenScriptPath;

	QString scriptsLogoutValue = settings.value("scripts/logout", "").toString();
	config scriptsLogout("scripts", "logout", scriptsLogoutValue);
	if (logoutScriptPath != "")
		scriptsLogout.value = logoutScriptPath;
	
	QString scriptsPoweroffValue = settings.value("scripts/poweroff", "").toString();
	config scriptsPoweroff("scripts", "poweroff", scriptsPoweroffValue);
	if (poweroffScriptPath != "")
		scriptsPoweroff.value = poweroffScriptPath;
	
	QString scriptsRebootValue = settings.value("scripts/reboot", "").toString();
	config scriptsReboot("scripts", "reboot", scriptsRebootValue);
	if (rebootScriptPath != "")
		scriptsReboot.value = rebootScriptPath;
	
	QString scriptsSuspendValue = settings.value("scripts/suspend", "").toString();
	config scriptsSuspend("scripts", "suspend", scriptsSuspendValue);
	if (suspendScriptPath != "")
		scriptsSuspend.value = suspendScriptPath;

	QString poweroffServiceValue = settings.value("poweroff/service", "").toString();
	config poweroffService("poweroff", "service", poweroffServiceValue);

	QString poweroffPathValue = settings.value("poweroff/path", "").toString();
	config poweroffPath("poweroff", "path", poweroffPathValue);

	QString poweroffInterfaceValue = settings.value("poweroff/interface", "").toString();
	config poweroffInterface("poweroff", "interface", poweroffInterfaceValue);

	QString poweroffMethodValue = settings.value("poweroff/method", "").toString();
	config poweroffMethod("poweroff", "method", poweroffMethodValue);

	QString rebootServiceValue = settings.value("reboot/service", "").toString();
	config rebootService("reboot", "service", rebootServiceValue);

	QString rebootPathValue = settings.value("reboot/path", "").toString();
	config rebootPath("reboot", "path", rebootPathValue);

	QString rebootInterfaceValue = settings.value("reboot/interface", "").toString();
	config rebootInterface("reboot", "interface", rebootInterfaceValue);

	QString rebootMethodValue = settings.value("reboot/method", "").toString();
	config rebootMethod("reboot", "method", rebootMethodValue);

	QString suspendServiceValue = settings.value("suspend/service", "").toString();
	config suspendService("suspend", "service", suspendServiceValue);

	QString suspendPathValue = settings.value("suspend/path", "").toString();
	config suspendPath("suspend", "path", suspendPathValue);

	QString suspendInterfaceValue = settings.value("suspend/interface", "").toString();
	config suspendInterface("suspend", "interface", suspendInterfaceValue);

	QString suspendMethodValue = settings.value("suspend/method", "").toString();
	config suspendMethod("suspend", "method", suspendMethodValue);

	QString batteryServiceValue = settings.value("battery/service", "").toString();
	config batteryService("battery", "service", batteryServiceValue);

	QString batteryPathValue = settings.value("battery/path", "").toString();
	config batteryPath("battery", "path", batteryPathValue);

	QString batteryInterfaceValue = settings.value("battery/interface", "").toString();
	config batteryInterface("battery", "interface", batteryInterfaceValue);

	QString batteryInformationPropertiesServiceValue = settings.value("battery:information-properties/service", "").toString();
	config batteryInformationPropertiesService("battery:information-properties", "service", batteryInformationPropertiesServiceValue);
	QString batteryInformationPropertiesPathValue = settings.value("battery:information-properties/path", "").toString();
	config batteryInformationPropertiesPath("battery:information-properties", "path", batteryInformationPropertiesPathValue);

	QString batteryInformationPropertiesInterfaceValue = settings.value("battery:information-properties/interface", "").toString();
	config batteryInformationPropertiesInterface("battery:information-properties", "interface", batteryInformationPropertiesInterfaceValue);

	QString batteryInformationPropertiesMethodValue = settings.value("battery:information-properties/method", "").toString();
	config batteryInformationPropertiesMethod("battery:information-properties", "method", batteryInformationPropertiesMethodValue);

	QString batteryisPluggedInServiceValue = settings.value("battery:is-plugged-in/service", "").toString();
	config batteryisPluggedInService("battery:is-plugged-in", "service", batteryisPluggedInServiceValue);
	QString batteryisPluggedInPathValue = settings.value("battery:is-plugged-in/path", "").toString();
	config batteryisPluggedInPath("battery:is-plugged-in", "path", batteryisPluggedInPathValue);

	QString batteryisPluggedInInterfaceValue = settings.value("battery:is-plugged-in/interface", "").toString();
	config batteryisPluggedInInterface("battery:is-plugged-in", "interface", batteryisPluggedInInterfaceValue);

	QString batteryisPluggedInMethodValue = settings.value("battery:is-plugged-in/method", "").toString();
	config batteryisPluggedInMethod("battery:is-plugged-in", "method", batteryisPluggedInMethodValue);

	QString networkingServiceValue = settings.value("networking/service", "").toString();
	config networkingService("networking", "service", networkingServiceValue);

	QString networkingPathValue = settings.value("networking/path", "").toString();
	config networkingPath("networking", "path", networkingPathValue);

	QString networkingInterfaceValue = settings.value("networking/interface", "").toString();
	config networkingInterface("networking", "interface", networkingInterfaceValue);

	QString networkingDeviceValue = settings.value("networking/device", "").toString();
	config networkingDevice("networking", "device", networkingDeviceValue);

	MainWindow::configuration.append(widgetApps);
	MainWindow::configuration.append(widgetClock);
	MainWindow::configuration.append(widgetBattery);
	MainWindow::configuration.append(widgetPower);
	MainWindow::configuration.append(widgetNetworking);
	MainWindow::configuration.append(scriptsLockScreen);
	MainWindow::configuration.append(scriptsLogout);
	MainWindow::configuration.append(scriptsPoweroff);
	MainWindow::configuration.append(scriptsReboot);
	MainWindow::configuration.append(scriptsSuspend);
	MainWindow::configuration.append(poweroffService);
	MainWindow::configuration.append(poweroffPath);
	MainWindow::configuration.append(poweroffInterface);
	MainWindow::configuration.append(poweroffMethod);
	MainWindow::configuration.append(rebootService);
	MainWindow::configuration.append(rebootPath);
	MainWindow::configuration.append(rebootInterface);
	MainWindow::configuration.append(rebootMethod);
	MainWindow::configuration.append(suspendService);
	MainWindow::configuration.append(suspendPath);
	MainWindow::configuration.append(suspendInterface);
	MainWindow::configuration.append(suspendMethod);
	MainWindow::configuration.append(batteryService);
	MainWindow::configuration.append(batteryPath);
	MainWindow::configuration.append(batteryInterface);
	MainWindow::configuration.append(batteryInformationPropertiesService);
	MainWindow::configuration.append(batteryInformationPropertiesPath);
	MainWindow::configuration.append(batteryInformationPropertiesInterface);
	MainWindow::configuration.append(batteryInformationPropertiesMethod);
	MainWindow::configuration.append(batteryisPluggedInService);
	MainWindow::configuration.append(batteryisPluggedInPath);
	MainWindow::configuration.append(batteryisPluggedInInterface);
	MainWindow::configuration.append(batteryisPluggedInMethod);
	MainWindow::configuration.append(networkingService);
	MainWindow::configuration.append(networkingPath);
	MainWindow::configuration.append(networkingInterface);
}

int MainWindow::getActiveConnectionStrength(QString SSID)
{
	int strength = -1;
	if (SSID == "")
		return strength;

	// Alternively, call findAPs() and then search?
	QSettings settings;
	const QString service = settings.value("networking/service", "org.freedesktop.NetworkManager").toString();
	const QString path = settings.value("networking/path", "/org/freedesktop/NetworkManager/AccessPoint").toString();
	const QString interface = settings.value("networking/interface", "org.freedesktop.NetworkManager.AccessPoint").toString();

	QDBusConnection bus = QDBusConnection::systemBus();

	QDBusMessage message = QDBusMessage::createMethodCall(service, path, "org.freedesktop.DBus.Introspectable", "Introspect");
	QDBusMessage rc = bus.call(message);
	QString xml = rc.arguments()[0].value<QString>();

	QXmlStreamReader reader(xml);
	while (!reader.atEnd() && !reader.hasError())
	{
		if (reader.readNext() == QXmlStreamReader::StartElement && reader.name() == "node")
		{
			QString accessPointID = reader.attributes().value("name").toString();
			if (accessPointID != "")
			{
				QString accessPointPath = path + "/" + accessPointID;

				// Prepare to retrieve properties
				QList<QVariant> args;
				args.append(QVariant(interface));
				QDBusMessage message, rc;

				// Retrieve signal strength, in percent
				// Higher numbers mean stronger signal
				args.clear();
				args.append(QVariant(interface));
				args.append(QVariant("Strength"));
				message = QDBusMessage::createMethodCall(service, accessPointPath, "org.freedesktop.DBus.Properties", "Get");
				message.setArguments(args);
				rc = bus.call(message);
				strength = rc.arguments()[0].value<QDBusVariant>().variant().value<int>();
			}
		}
	}

	return strength;
}

void MainWindow::connectToNetwork(QString ssid)
{
	// Passing all the options to this slot is hard
	// Instead, use the SSID to search through all options and find the correct one
	QWidget *widget;
	QString username = "",
			password = "",
			security = "",
			device = readConfig(MainWindow::configuration, "Networking/device", "");

	for (int i = 0; i < this->accessPointOptions->layout()->count(); ++i)	
	{
		widget = this->accessPointOptions->layout()->itemAt(i)->widget();
		QString accessPointName = widget->accessibleName();

		if (ssid.compare(accessPointName) == 0)	
		{
			QWidget *usernameWidget = widget->layout()->itemAt(1)->widget();
			QWidget *passwordWidget = widget->layout()->itemAt(2)->widget();
			QWidget *securityWidget = widget->layout()->itemAt(3)->widget();
			username = ((QLineEdit*)usernameWidget)->text();
			password = ((QLineEdit*)passwordWidget)->text();
			security = ((QComboBox*)securityWidget)->currentText();

			break;
		}		
	}

	// Add and connect
	QProcess *nmcli = new QProcess(this);
	nmcli->start("nmcli", QStringList() << "con add con-name '" << ssid << "' ifname " << device << "type wifi ssid '" << ssid << "'");

	if (security.compare("WEP") == 0)
	{
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.key-mgmt none");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.key-0 " << password);
	}

	if (security.compare("Dynamic WEP") == 0)
	{
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.key-mgmt ieee8021x");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.key-0 " << password);
	}

	if (security.compare("WPA-PSK") == 0)
	{
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.key-mgmt wpa-psk");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.psk " << password);
	}

	if (security.compare("Ad-Hoc WPA-PSK") == 0)
	{
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.key-mgmt wpa-none");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.psk " << password);
	}

	if (security.compare("WPA Enterprise") == 0)
	{
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' 802-1x.eap peap");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' 802-1x.identity '" << username << "'");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' 802-1x.phase2-auth mschapv2");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.key-mgmt wpa-eap");
		if (nmcli->waitForFinished())
			nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.leap-username" << username);
		nmcli->start("nmcli", QStringList() << "con mod '" << ssid << "' wifi-sec.leap-password" << password);
	}

	// Note: may need to run this in terminal
	if (nmcli->waitForFinished())
		nmcli->start("nmcli", QStringList() << "con up '" << ssid << "'"); 
}
