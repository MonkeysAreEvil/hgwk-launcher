/***************************************************************************
 * file: main-window.h													   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: boilerplate that defines the mainWindow class.				   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef MainWindow_H
#define MainWindow_H

#include "accessPoint.h"
#include "config.h"
#include "networkDevice.h"
#include <QWidget>
#include <QMainWindow>
#include <QCalendarWidget>
#include <QComboBox>
#include <QCommandLineParser>
#include <QCompleter>
#include <QDBusInterface>
#include <QFileSystemWatcher>
#include <QGraphicsDropShadowEffect>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QListWidget>
#include <QLineEdit>
#include <QPixmap>
#include <QPushButton>
#include <QScrollArea>
#include <QSignalMapper>
#include <QStackedWidget>
#include <QStringList>
#include <QTableView>
#include <QTimer>
#include <QVBoxLayout>
#include <map>

class MainWindow : public QMainWindow
{
Q_OBJECT
public:
    MainWindow(bool, bool, bool, bool, bool, QString, QString, QString, QString, QString);
    virtual ~MainWindow();
	QList<accessPoint> findAPs();
	QList<config> configuration;
	QString configPath;
	QString home;

private slots:
	void appsChanged(QString);
	void batteryGetInfo();
	void clearSearch();
	void configChanged(QString);
	void connectToNetwork(QString);
	void filterShortcuts();
	void launchShortcut(QString);
	void lock();
	void logout();
	void powerOff();
	void reboot();
	void searchText();
	void setOptionsWidget(QListWidgetItem *);
	void suspend();
	void toggleNetworkingOptions(bool);
	void updateClock();
	void updateClockButton();
	void updateDateLabel();
	void updateTimeLabel();

protected:
	bool eventFilter(QObject *obj, QEvent *event);

private:
	bool isPluggedIn();
	double convertTimeToDecimal(QTime);
	double getBatteryCapacity();
	double getBatteryEnergy();
	double getBatteryEnergyFull();
	double getBatteryEnergyFullDesign();
	double getBatteryEnergyRate();
	double getBatteryPercentage();
	double getBatteryVoltage();
	int64_t getBatteryTimeToEmpty();
	int64_t getBatteryTimeToFull();
	int getActiveConnectionStrength(QString);
	QDBusMessage getBatteryInformation(QString);
	QList<networkDevice> findNetworkDevices();
	QString getActiveConnectionPath();
	QString getActiveConnectionSSID(QString);
	QString getAppIcon(QString);
	QString getGateway();
	QString getIPAddress();
	QString getNetMask();
	QStringList buildCompleter();
	QString readConfig(QList<config>, QString, QString);
	QTime convertNumbersToTime(int64_t, int64_t, int64_t);
	std::string exec(std::string, bool);
	std::string updateTime(std::string);
	void buildApps();
	void buildAP(QStringList);
	void buildBattery();
	void buildClock();
	void buildConfig(bool, bool, bool, bool, bool, QString, QString, QString, QString, QString);
	void buildNetworking();
	void buildPower();
	void buildUI();
	void getBatteryInfo();
	void getWifiState();
	void setNetworkIcon();
	QCalendarWidget *calendar;
	QCommandLineParser parser;
	QCompleter *completer;
	QFileSystemWatcher *appsWatcher;
	QFileSystemWatcher *configWatcher;
	QFrame *line;
	QFrame *line2;
	QGridLayout *networkContainerLayout;
	QHBoxLayout *centralLayout;
	QHBoxLayout *searchLayout;
	QLabel *batteryLargeWidget;
	QLabel *dateLabel;
	QLabel *gatewayLabel;
	QLabel *ipAddressLabel;
	QLabel *netmaskLabel;
	QLabel *timeLabel;
	QLineEdit *gateway;
	QLineEdit *ipAddress;
	QLineEdit *netmask;
	QLineEdit *searchBox;
	QListWidget *accessPointList;
	QPushButton *appsButton;
	QPushButton *batteryButton;
	QPushButton *clearButton;
	QPushButton *clockButton;
	QPushButton *configStaticButton;
	QPushButton *connectButton;
	QPushButton *lockButton;
	QPushButton *logoutButton;
	QPushButton *networkingButton;
	QPushButton *powerButton;
	QPushButton *powerOffButton;
	QPushButton *rebootButton;
	QPushButton *reconnectButton;
	QPushButton *resetDHCPButton;
	QPushButton *saveNetworkOptions;
	QPushButton *ssidScanButton;
	QPushButton *suspendButton;
	QRadioButton *dhcpStatic;
	QScrollArea *scrollAreaAPs;
	QScrollArea *shortcutsScrollArea;
	QScrollArea *ssidScrollArea;
	QSignalMapper *appsButtonSM;
	QSignalMapper *apSignalMapper;
	QSignalMapper *batteryButtonSM;
	QSignalMapper *clockButtonSM;
	QSignalMapper *configStaticButtonSM;
	QSignalMapper *connectButtonSM;
	QSignalMapper *networkingButtonSM;
	QSignalMapper *networksSM;
	QSignalMapper *powerButtonSM;
	QStackedWidget *accessPointOptions;
	QStackedWidget *networkingExtraWidget;
	QStackedWidget *networkingStack;
	QStackedWidget *networks;
	QStackedWidget *rightPanel;
	QTimer *batteryTimer;
	QTimer *clockTimer;
	QTimer *dateLabelTimer;
	QTimer *timeLabelTimer;
	QVBoxLayout *accessPointOptionLayout;
	QVBoxLayout *appsExtraLayout;
	QVBoxLayout *batteryExtraLayout;
	QVBoxLayout *clockExtraLayout;
	QVBoxLayout *leftLayout;
	QVBoxLayout *networkingOptionsLayout;
	QVBoxLayout *powerExtraLayout;
	QVBoxLayout *shortcutsLayout;
	QVBoxLayout *ssidLayout;
	QWidget *appsExtraWidget;
	QWidget *batteryExtraWidget;
	QWidget *centralWidget;
	QWidget *clockExtraWidget;
	QWidget *configureStaticPage;
	QWidget *leftPanel;
	QWidget *networkContainer;
	QWidget *networkingOptions;
	QWidget *powerExtraWidget;
	QWidget *searchWidget;
	QWidget *secondPage;
	QWidget *shortcutsWidget;
};

// Need to register this type for D-Bus (getBatteryIconName())
Q_DECLARE_METATYPE(std::string)

#endif // MainWindow_H
