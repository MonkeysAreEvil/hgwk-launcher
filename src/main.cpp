/***************************************************************************
 * file: main.cpp														   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: A minimal app launcher for GNU/Linux. This build should be 	   *
 * 		  considered a  beta release and is intended for general use.	   *
 * 		  This file is the main entry point for the application, loading   *
 * 		  the GUI object and defining command line options.				   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/
/*

   Copyright (C) 2016 MonkeysAreEvil

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */

#include <QApplication>
#include <QStringList>
#include "main-window.h"
#include <iostream>


int main(int argc, char** argv)
{
	// Need this to make getBatteryIconName() to work
	qRegisterMetaType<std::string>();

	// Define the GUI object.
	QApplication app(argc, argv);
	app.setApplicationName("hgwk-launcher");
	app.setApplicationVersion("0.1");

	// Define the command line options.
	QCommandLineParser parser;
	parser.setApplicationDescription("hgwk-launcher is a WM/DE agnostic application launcher and desktop monitor. This application should be considered a beta version and for general used. There are options to disable unwanted widgets. You must have a config directory set up to use hgwk launcher. The slprz options override the dbus settings in the rc file.");
	parser.addHelpOption();
	parser.addVersionOption();

	QCommandLineOption appsOption(QStringList() << "a" << "disable-apps", QCoreApplication::translate("main", "Don't display the app launcher."));
	QCommandLineOption batteryOption(QStringList() << "b" << "disable-battery", QCoreApplication::translate("main", "Don't display the battery info."));
	QCommandLineOption clockOption(QStringList() << "c" << "disable-clock", QCoreApplication::translate("main", "Don't display the clock and calender."));
	QCommandLineOption powerOption(QStringList() << "p" << "disable-power", QCoreApplication::translate("main", "Don't display the power buttons."));
	QCommandLineOption networkingOption(QStringList() << "w" << "disable-networking", QCoreApplication::translate("main", "Don't display the networking config and info."));
	QCommandLineOption lockScreenOption(QStringList() << "s" << "lock-screen",
			QCoreApplication::translate("main", "Path to lock screen script."),
			QCoreApplication::translate("main", "path"));
	QCommandLineOption logoutOption(QStringList() << "l" << "logout",
			QCoreApplication::translate("main", "Path to logout script."),
			QCoreApplication::translate("main", "path"));
	QCommandLineOption poweroffOption(QStringList() << "p" << "poweroff",
			QCoreApplication::translate("main", "Path to poweroff script."),
			QCoreApplication::translate("main", "path"));
	QCommandLineOption rebootOption(QStringList() << "r" << "reboot",
			QCoreApplication::translate("main", "Path to reboot script."),
			QCoreApplication::translate("main", "path"));
	QCommandLineOption suspendOption(QStringList() << "z" << "suspend",
			QCoreApplication::translate("main", "Path to suspend script."),
			QCoreApplication::translate("main", "path"));

	parser.addOption(appsOption);
	parser.addOption(batteryOption);
	parser.addOption(clockOption);
	parser.addOption(powerOption);
	parser.addOption(networkingOption);
	parser.addOption(lockScreenOption);
	parser.addOption(logoutOption);
	parser.addOption(poweroffOption);
	parser.addOption(rebootOption);
	parser.addOption(logoutOption);
	parser.addOption(suspendOption);

	parser.process(app);

	// Process the command line options and pass them to the mainWindow object.
	const QStringList args = parser.positionalArguments();

	bool apps = parser.isSet(appsOption);
	bool battery = parser.isSet(batteryOption);
	bool clock = parser.isSet(clockOption);
	bool power = parser.isSet(powerOption);
	bool networking = parser.isSet(networkingOption);
	QString lockScreen = parser.value(lockScreenOption);
	QString logout = parser.value(logoutOption);
	QString poweroff = parser.value(poweroffOption);
	QString reboot = parser.value(rebootOption);
	QString suspend = parser.value(suspendOption);

	MainWindow mainWindow(apps, battery, clock, power, networking, lockScreen, logout, poweroff, reboot, suspend);

	mainWindow.show();


	// Run the application
	return app.exec();
}
