/***************************************************************************
 * file: networkDevice.h												   *
 * project: hgwk-launcher												   *
 * url: https://bitbucket.org/MonkeysAreEvil/hgwk-launcher				   *
 * notes: build the gui and do all the main things						   *
 * Copyright (C) 2017 by MonkeysAreEvil <monkey@etage.io> 				   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef NETWORK_DEVICE_H
#define NETWORK_DEVICE_H

#include <QtWidgets>

enum types
{
	NM_DEVICE_TYPE_UNKNOWN,
	NM_DEVICE_TYPE_ETHERNET,
	NM_DEVICE_TYPE_WIFI,
	NM_DEVICE_TYPE_UNUSED1,
	NM_DEVICE_TYPE_UNUSED2,
	NM_DEVICE_TYPE_BT,
	NM_DEVICE_TYPE_OLPC_MESH,
	NM_DEVICE_TYPE_WIMAX,
	NM_DEVICE_TYPE_MODEM,
	NM_DEVICE_TYPE_INFINIBAND,
	NM_DEVICE_TYPE_BOND,
	NM_DEVICE_TYPE_VLAN,
	NM_DEVICE_TYPE_ADSL,
	NM_DEVICE_TYPE_BRIDGE,
	NM_DEVICE_TYPE_TEAM,
	NM_DEVICE_TYPE_TUN,
	NM_DEVICE_TYPE_IP_TUNNEL,
	NM_DEVICE_TYPE_MACVLAN,
	NM_DEVICE_TYPE_VXLAN,
	NM_DEVICE_TYPE_VETH,
	NM_DEVICE_TYPE_MACSEC
};

class networkDevice
{
	public:
		networkDevice();
		virtual ~networkDevice();
		int type;
		bool connected;
		int strength;
};

#endif
